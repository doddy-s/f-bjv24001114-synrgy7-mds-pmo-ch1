package com.doddysujatmiko.entities;

public class Dish {
    public String name;
    public int price;

    public Dish(String name, int price) {
        this.name = name;
        this.price = price;
    }
}
