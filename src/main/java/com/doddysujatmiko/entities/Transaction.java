package com.doddysujatmiko.entities;

public class Transaction {
    public Dish dish;
    public int amount;

    public Transaction(Dish dish, int amount) {
        this.dish = dish;
        this.amount = amount;
    }
}
