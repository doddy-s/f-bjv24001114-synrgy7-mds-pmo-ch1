package com.doddysujatmiko;

import com.doddysujatmiko.entities.Dish;
import com.doddysujatmiko.entities.Transaction;

import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class App {
    private final Scanner input;

    private boolean shouldEnd = false;

    private String menu;

    private String view;

    private Dish[] dishes = {
            new Dish("Nasi Goreng", 15000),
            new Dish("Mie Goreng", 13000),
            new Dish("Nasi Ayam", 10000),
            new Dish("Es Teh", 4000),
            new Dish("Es Jeruk", 5000)
    };

    private List<Transaction> transactions = new ArrayList<>();

    public App(Scanner input) {
        this.input = input;
        drawMenu();
    }

    private void drawMenu() {
        StringBuilder builder = new StringBuilder();

        builder.append("Silahkan pilih makanan:\n");
        for(int i = 0; i< dishes.length; i++) {
            builder
                    .append((i + 1))
                    .append(". ")
                    .append(dishes[i].name)
                    .append("\t|\t")
                    .append(dishes[i].price)
                    .append('\n');
        }

        menu = builder.toString();
    }

    private void render() {
        System.out.print(view);
    }

    private void order(int x) {
        view = "==============================\n" +
                "Berapa Pesanan Anda\n" +
                "==============================\n" +
                dishes[x].name +
                "\t|\t" +
                dishes[x].price +
                "\n(input 0 untuk kembali)\n" +
                "qty => ";
        render();

        int amount = input.nextInt();
        if(amount < 1) return;

        transactions.add(new Transaction(dishes[x], amount));
    }

    private void pay() {
        int pilihan;
        int totalHarga = 0;
        view = "==============================\n" +
                "Konfirmasi dan Pembayaran\n" +
                "==============================\n";

        StringBuilder builder = new StringBuilder();
        for(int i = 0; i< transactions.size(); i++) {
            Transaction temp = transactions.get(i);
            totalHarga += temp.amount * temp.dish.price;
            builder
                    .append((i + 1))
                    .append(". ")
                    .append(temp.dish.name)
                    .append("\t|\t")
                    .append(temp.amount)
                    .append("\t|\t")
                    .append(temp.amount * temp.dish.price)
                    .append('\n');
        }

        view += builder.toString();
        view += "-------------------------------+\n" +
                "Total\t|\t" + totalHarga + "\n";
        view += "1. Konfirmasi dan bayar\n" +
                "2. Kembali ke menu utama\n" +
                "0. Keluar Aplikasi" +
                "=> ";
        render();
        pilihan = input.nextInt();
        if(pilihan == 1) {
            System.out.print("Nama file?");
            input.nextLine();
            String fileName = input.nextLine();
            saveStringAsTxt(view, fileName + ".txt");
            transactions.clear();
        } else if(pilihan == 2) {
            return;
        } else if (pilihan == 0) {
            shouldEnd = true;
        }
    }

    public static void saveStringAsTxt(String content, String filePath) {
        try {
            FileWriter writer = new FileWriter(filePath);
            writer.write(content);
            writer.close();
            System.out.println("Struk pembayaran berhasil disimpan.");
        } catch (IOException e) {
            System.err.println("An error occurred while saving the string to file: " + e.getMessage());
            e.printStackTrace();
        }
    }

    public void run() {
        int pilihan;
        String header = "==============================\n" +
                "Selamat Datang di BinarFud\n" +
                "==============================\n";
        String navChoices = "99. Pesan dan bayar\n" +
                "0. Keluar Aplikasi\n";
        while (!shouldEnd) {


            view = header + menu + navChoices + "=> ";
            render();
            pilihan = input.nextInt();
            if(pilihan == 0) {
                shouldEnd = true;
            } else if (pilihan == 99) {
                pay();
            } else if(pilihan > 0 && pilihan <= dishes.length){
                order(pilihan-1);
            } else {
                System.out.print("Input salah tekan Enter!");
                input.nextLine();
                input.nextLine();
            }
        }
    }
}
